trigger AccountTest on Account (before update, After insert) {
    List<Account> lstAccs = new List<Account>();
    List<Account> lstUpdatedAccs = new List<Account>();
    for(Account acc : Trigger.new){
        if(Trigger.isBefore && Trigger.isUpdate){
            lstUpdatedAccs.add(acc);
        }else if(Trigger.isAfter && Trigger.isInsert){
            lstAccs.add(acc);
        }else{}
    }
    system.debug('lstUpdatedAccs  ==>'+lstUpdatedAccs);
    if((lstUpdatedAccs != null && lstUpdatedAccs.size() > 0)){
        system.debug('Inside Before block');
        AccountTest.updateAcc(lstUpdatedAccs);
    }
    if((lstAccs != null && lstAccs.size() >0)){
    system.debug('Inside After If block');
        AccountTest.updateNewAccs(lstAccs);
    }
}