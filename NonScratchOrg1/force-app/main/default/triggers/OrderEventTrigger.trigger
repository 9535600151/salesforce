trigger OrderEventTrigger on Order_Event__e (after insert) {
    List<Task> lstTasks = new List<Task>();
    for(Order_Event__e event: Trigger.new){
        if(event.Has_Shipped__c == true){
            Task task = new Task();
            task.Priority = 'Medium';
            task.Subject = 'Follow up on shipped order '+event.Order_Number__c;
            task.ownerId = event.CreatedById;
            lstTasks.add(task);
        }
    }
    
    if(lstTasks != null && lstTasks.size() > 0 ){
        insert lstTasks;
    }
}