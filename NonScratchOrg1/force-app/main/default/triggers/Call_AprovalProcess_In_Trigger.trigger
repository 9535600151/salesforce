trigger Call_AprovalProcess_In_Trigger on Account (before update) {
 set<Id> setOfUserIds = new set<Id>();
 for(GroupMember gm : [select Id,userorgroupId from GroupMember where group.name = 'Interviewer Approvers']){
     if(gm.userorgroupId != null){
         setOfUserIds.add(gm.userorgroupId);
     }
 } 
 for(Account acc:trigger.new){
    if(acc.AnnualRevenue < 2000){
       approval.ProcessSubmitRequest aprlPrcs = new Approval.ProcessSubmitRequest();     
       aprlPrcs .setComments('Submitting record for approval.');
       aprlPrcs.setObjectId(acc.id);
       approval.ProcessResult result = Approval.process(aprlPrcs);
    }
 }
}