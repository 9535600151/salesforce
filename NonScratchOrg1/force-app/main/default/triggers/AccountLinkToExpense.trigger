trigger AccountLinkToExpense on Expense__c (Before Insert, Before Update) {
    set<String> setOfStrings = new set<String>();
    Map<String,String> mapOfStr = new Map<String,String>();
    for(Expense__c op : Trigger.new){
        setOfStrings.add(op.External_Id__c);
    }
    for(Account acc : [select Id,AccountNumber from Account where AccountNumber IN:setOfStrings]){
        mapOfStr.put(acc.AccountNumber,acc.Id);
    }
    for(Expense__c opp : Trigger.new){
        opp.Account__c = mapOfStr.get(opp.External_Id__c);
    }
}