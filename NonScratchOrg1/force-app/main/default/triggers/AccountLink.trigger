trigger AccountLink on Opportunity (Before Insert, Before Update) {
    set<String> setOfStrings = new set<String>();
    Map<String,String> mapOfStr = new Map<String,String>();
    for(Opportunity op : Trigger.new){
        op.Name='Test 123';
        setOfStrings.add(op.External_Id__c);
    }
    for(Account acc : [select Id,AccountNumber from Account where AccountNumber IN:setOfStrings]){
        mapOfStr.put(acc.AccountNumber,acc.Id);
    }
    for(Opportunity opp : Trigger.new){
        opp.AccountId = mapOfStr.get(opp.External_Id__c);
    }
}