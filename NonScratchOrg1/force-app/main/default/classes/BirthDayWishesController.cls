public class BirthDayWishesController {
    //This method check for birthday with today's date. If condition get matched then it retues the true.
    @AuraEnabled
    public static boolean wishTheBirthDay(){
        User currentLoggedinUser = [Select id,Date_of_Birth__c from User Where Id =: UserInfo.getUserId() ];
        
    if( currentLoggedinUser.Date_of_Birth__c != null ){
            if( Date.today().Day() == currentLoggedinUser.Date_of_Birth__c.Day() &&
               Date.today().Month() == currentLoggedinUser.Date_of_Birth__c.Month()){
                   return true;
               }
        }
        return false;
    }
}