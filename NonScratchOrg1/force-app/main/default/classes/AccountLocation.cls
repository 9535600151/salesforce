public class AccountLocation {
    @AuraEnabled
    public static List<String> publishVehicleIds(Integer offset){
        List<Publish_Vehicle_ID__e> newsEventList = new List<Publish_Vehicle_ID__e>();
        List<String> lstOfVehicleIds = new List<String>();
        for(Vehicle_Info__c vehicleInfo : [select Id,Vehicle_ID__c from Vehicle_Info__c limit 2 OFFSET:integer.valueOf(offset)]){
            Publish_Vehicle_ID__e publishVehicleId = new Publish_Vehicle_ID__e();
            publishVehicleId.Vehicle_ID__c = vehicleInfo.Vehicle_ID__c;
            newsEventList.add(publishVehicleId);
            lstOfVehicleIds.add(vehicleInfo.Vehicle_ID__c);
        }
        List<Database.SaveResult> results = EventBus.publish(newsEventList);
        return lstOfVehicleIds;
    }
    @AuraEnabled
    public static List<String> publishVehicleId(String vehicleId){
        system.debug('vehicleId==>'+vehicleId);
        Map<String,Object> mapOfVehicleIds = (Map<String,Object>)JSON.deserializeUntyped(vehicleId);
        String vehicle = String.valueOf(mapOfVehicleIds.get('Vehicle_ID__c'));
        List<String> lstOfVehicleIds = new List<String>();
        if(String.isNotBlank(vehicle)){
            List<Publish_Vehicle_ID__e> newsEventList = new List<Publish_Vehicle_ID__e>();
            Publish_Vehicle_ID__e publishVehicleId = new Publish_Vehicle_ID__e();
            publishVehicleId.Vehicle_ID__c = vehicle;
            newsEventList.add(publishVehicleId);
            lstOfVehicleIds.add(vehicleId);
            List<Database.SaveResult> results = EventBus.publish(newsEventList);
        }
        return lstOfVehicleIds;
    }
    @AuraEnabled
    public static list<Vehicle_Info__c> retrieveVehicles(string searchText){
        list<Vehicle_Info__c> vehicles = New list<Vehicle_Info__c>();
        searchText = '%'+searchText+'%';
        vehicles = [select Id,Vehicle_ID__c,Name from Vehicle_Info__c where Vehicle_ID__c like : searchText];
        return vehicles;
    }
    @AuraEnabled
    public static List<CarAddressWrapper> getVehicleDetails(String lstOfEventMessages){
        List<Object> platformEventMessages = (List<Object>) JSON.deserializeUntyped(lstOfEventMessages);
        system.debug('platformEventMessages ==>'+platformEventMessages);
        Map<String, Object> mapOfEventMessages;
        Map<String,Object> mapOfPayloadData;
        map<String,Object> mapOfVehicleData;
        List<CarAddressWrapper> lstOfCarAddWrappers = new List<CarAddressWrapper>();
        for(Object obj : platformEventMessages){
            mapOfEventMessages = (Map<String,Object>)obj;
            system.debug('mapOfEventMessages ==>'+(Map<String,Object>)mapOfEventMessages.get('data'));
            mapOfPayloadData = (Map<String,Object>)mapOfEventMessages.get('data');
            system.debug('mapOfPayloadData ==>'+mapOfPayloadData.get('payload'));
            mapOfVehicleData = (Map<String,Object>)mapOfPayloadData.get('payload');
            system.debug('mapOfVehicleData ==>'+mapOfVehicleData.get('Vehicle_Location__c'));
            String vehicleLocation = String.valueOf(mapOfVehicleData.get('Vehicle_Location__c'));
            	
                CarAddressWrapper carAddress = new CarAddressWrapper();
                carAddress.icon = 'custom:custom31';
                carAddress.title = String.valueOf(mapOfVehicleData.get('Vehicle_ID__c'));
                carAddress.description = String.valueOf(mapOfVehicleData.get('Vehicle_ID__c'));
                AddressWrapper addWrap = new AddressWrapper();
                if(string.isNotBlank(vehicleLocation)){
                    list<string> addressList = new list<string>();
                    addressList = vehicleLocation.split(',');
                    system.debug('vehicleLocation ==>'+addressList);
                    if(addressList.size() >= 4)
                        addWrap.Street = addressList[addressList.size() - 4].trim();
                    if(addressList.size() >= 3)
                        addWrap.City = addressList[addressList.size() - 3].trim();
                    if(addressList.size() >= 2)
                        addWrap.state = addressList[addressList.size() - 2].trim();
                    if(addressList.size() >= 1)
                        addWrap.country = addressList[addressList.size() - 1].trim();
                    system.debug('addWrap ==>'+addWrap);
                    carAddress.location = addWrap;
                }
                lstOfCarAddWrappers.add(carAddress);
        }
        
		return lstOfCarAddWrappers;
    }
    
/**
	* CarAddressWrapper class which will hold icon and title of marker
	* along with multiple locations
**/
    public class CarAddressWrapper{
        @AuraEnabled public String title;
        @AuraEnabled public String icon;
        @AuraEnabled public String description;
        @AuraEnabled public AddressWrapper location;
    }
/**
	* AddressWrapper class to hold address properties
**/
    public class AddressWrapper{
        @AuraEnabled public String Street;
        @AuraEnabled public String City;
        @AuraEnabled public String State;
        @AuraEnabled public String Country;
        //@AuraEnabled public String PostalCode;
        //@AuraEnabled public Decimal Latitude;
        //@AuraEnabled public Decimal Longitude;
    }
    
}