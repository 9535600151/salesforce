public class SendAccountUsingRESTAPI
{
    //---Class Variables
    private final String clientId = '3MVG9ZL0ppGP5UrCJUTSUwWqDWaLI7RbaJrjAAUogGcGi.iAY5wMazYEa3JIMckpyeOjRP4RJWWDvl4J5VSgt';
    private final String clientSecret = '9205318701576330610';
    private final String username = 'sekharmude@accenture.com';
    private final String password = 'Jai$hanuman520I5UhLGN2uomLNKv2W5Cvm5eA';
    
    //---Inner Class to be used for deserializing
    public class deserializeResponse
    {
        public String id;
        public String access_token;
        public String Name;
        public String Description;
    }
    public class deserializeConRes{
        public String Id;
        public String Name;
    }
    //---Class Method: For authentication and access token extraction, Uses Username-Pwd OAuth Authentication Flow
    public String ReturnAccessToken (SendAccountUsingRESTAPI acount)
    {
        String reqbody = 'grant_type=password&client_id='+clientId+'&client_secret='+clientSecret+'&username='+username+'&password='+password;        
        
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setBody(reqbody);
        req.setMethod('POST');
        req.setEndpoint('https://login.salesforce.com/services/oauth2/token');
        HttpResponse res = h.send(req);
        
        //---Researched in SFDC books: Method 2 for parsing JSON and extracting a Token value without looping, Uses a class as written above
        deserializeResponse resp1 = (deserializeResponse)JSON.deserialize(res.getbody(), deserializeResponse.class);        
        System.debug('Access Token = ' + resp1.access_token);
        System.debug('Access Token Response = ' + resp1);
        
        return resp1.access_token;
    }
    
    //---Class Method: For REST API call to the Target, uses access token extracted above, asynchronous
    @future(callout=true)
    public static void callcreateAcc (String conName) 
    {
        SendAccountUsingRESTAPI acount1 = new SendAccountUsingRESTAPI();
        String accessToken = acount1.ReturnAccessToken (acount1);
        
        if(accessToken != null){
        
            String endPoint = 'https://ap2.salesforce.com/services/apexrest/Contact/'+conName;       
            Http h1 = new Http();
            HttpRequest req1 = new HttpRequest();
            req1.setHeader('Authorization','Bearer ' + accessToken);
            req1.setHeader('Content-Type','application/json');
            req1.setHeader('accept','application/json');
            req1.setMethod('GET');
            req1.setEndpoint(endPoint);
            HttpResponse res1 = h1.send(req1);
            system.debug('This is res1 '+res1.getbody());
            List<deserializeConRes> resp2 = (List<deserializeConRes>)JSON.deserialize(res1.getbody(), List<deserializeConRes>.class);
            system.debug('This is resp2 '+resp2);
            List<Contact> lstContacts = new List<Contact>();
            for(deserializeConRes odeserializeConRes:resp2){ 
                Contact con = new Contact();
                con.Contact_Id__c = odeserializeConRes.Id;
                con.lastName = odeserializeConRes.Name;
                lstContacts.add(con);
            }
            if(lstContacts!=null && lstContacts.size()>0){
                insert lstContacts;
            }
        }
    }
}