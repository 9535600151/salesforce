public class AccountListView_Hacthon {
	@AuraEnabled
    public static List<Account> getAccounts(){
        return [select Id,Name,AccountNumber from Account limit 10];
    }
}