public class AccountTest{
    public static void updateAcc(List<Account> lstUpdatedTasks){
        for(Account acc : lstUpdatedTasks){
            acc.Type = 'Prospect';
            acc.Test_External_Id__c = acc.Type;
            system.debug('Inside Before Update');
        }
        
    }
    public static void updateNewAccs(List<Account> lstNewTasks){
        List<Account> lst = [select Id,Type,Test_External_Id__c  from Account where Name = 'Sekhar M'];
        Account acc = new Account(Id = lst[0].Id);
        acc.Type = 'Other';
        stopRecursion.stopBeforeTrigger = TRUE;
        update acc;
        system.debug('lst===>'+acc);
     }
}