public class GetCustomFieldsDataFromMetaData {
	@AuraEnabled
    public static List<String> getCustomFields(){
        Test_Edit_Form__mdt mdt = [select Id,Account_Custom_Fields__c from Test_Edit_Form__mdt limit 1];
        List<String> lstStr = new List<String>();
        if(mdt.Account_Custom_Fields__c != null){
            for(String str : mdt.Account_Custom_Fields__c.split(';')){
                lstStr.add(str);
            }
        }
        return lstStr;
    }
}